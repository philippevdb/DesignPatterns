package behavioral.templateMethod;

public class ConsumerOrderHandler implements OrderHandler {
    @Override
    public void createOrder() {
        System.out.println("Create order.");
    }

    @Override
    public void reserveStock() {
        System.out.println("Set stock aside if available.");
    }

    @Override
    public void createPicking() {
        System.out.println("Create picking.");
    }

    @Override
    public void createLabels() {
        System.out.println("Create labels, ready for printing.");
    }
}
