Use when:
<ul>
    <li>implement invariant parts of algorithm once and leave it up to subclasses to implement the behaviour that can vary.</li>
    <li>when common behaviour among subclasses should be factored and localized</li>
    <li>to control subclasses extensions</li>
</ul>

Related to:
<ul>
    <li>Factory method: often called by template methods</li>
    <li>Strategy: template methods use inheritance to vary part of an algorithm. Strategies use delegation to vary the entire algorithm.</li>
</ul>