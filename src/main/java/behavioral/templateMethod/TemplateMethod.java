package behavioral.templateMethod;

public class TemplateMethod {
    public static void main(String[] args) {
        OrderHandler consumerOrder = new ConsumerOrderHandler();
        consumerOrder.handleOrder();
    }
}
