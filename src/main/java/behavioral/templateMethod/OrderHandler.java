package behavioral.templateMethod;

public interface OrderHandler {

    void createOrder();
    void reserveStock();
    void createPicking();
    void createLabels();

    default void handleOrder() {
        createOrder();
        reserveStock();
        createPicking();
        createLabels();
    }
}
