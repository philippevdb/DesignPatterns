package behavioral.visitor;

public class ManagerEmailVisitor implements EmployeeVisitor {
    @Override
    public void visitManager(Manager manager) {
        System.out.println("Sending email to: " + manager);
    }

    @Override
    public void visitFloormen(Floorman floorman) {
        // do nothing
    }

}
