package behavioral.visitor;

public class Manager extends Employee {

    @Override
    public void accept(EmployeeVisitor visitor) {
        visitor.visitManager(this);
        super.accept(visitor);
    }

    @Override
    public String toString() {
        return "Manager";
    }
}
