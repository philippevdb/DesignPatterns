package behavioral.visitor;

public class Visitor {

    public static void main(String[] args) {
        Employee manager = new Manager();
        manager.setSubordinates(new Employee[] {new Floorman(), new Floorman(), new Floorman()});
        manager.accept(new ManagerEmailVisitor());
        manager.accept(new TeamBuildingVisitor());
    }
}
