package behavioral.visitor;

public class Floorman extends Employee {

    @Override
    public void accept(EmployeeVisitor visitor) {
        visitor.visitFloormen(this);
        super.accept(visitor);
    }

    @Override
    public String toString() {
        return "Floorman";
    }
}
