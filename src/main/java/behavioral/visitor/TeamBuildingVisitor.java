package behavioral.visitor;

public class TeamBuildingVisitor implements EmployeeVisitor {
    @Override
    public void visitManager(Manager manager) {
        // do nothing
    }

    @Override
    public void visitFloormen(Floorman floorman) {
        System.out.println("Send on teambuilding: " + floorman);
    }
}
