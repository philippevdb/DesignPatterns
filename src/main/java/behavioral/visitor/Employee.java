package behavioral.visitor;

import lombok.Data;

import java.util.Arrays;
import java.util.List;

@Data
public abstract class Employee {

    private Employee[] subordinates;

    public void accept(EmployeeVisitor visitor) {
        Arrays.stream(this.subordinates).forEach(subordinate -> subordinate.accept(visitor));
    }
}
