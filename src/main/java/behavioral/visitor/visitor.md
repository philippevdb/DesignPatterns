Use when:
<ul>
    <li>object structure contains many classes of objects with differing interfaces</li>
    <li>many distinct and unrelated operations need to be performed on objects in an object structure and you want to avoid pulluting their classes with these operations. Visitor keeps related operations together.</li>
    <li>classes defining object structure rarely change but you often want to define new operations voer the structure.</li>
</ul>

Consequences:
<ul>
    <li>visitor makes adding new operations easy</li>
    <li>visitor gathers related operations and separates unrelated ones</li>
    <li>adding new ConcreteElement classes is hard</li>
    <li>visiting across class hierarchies</li>
    <li>accumulating state</li>
    <li>breaking encapsulation</li>
</ul>

Related to:
<ul>
    <li>Composite: can be used to apply an operation over an object structure by composite pattern.</li>
    <li>visitor may be applied to do the interpretation.</li>
</ul>