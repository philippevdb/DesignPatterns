package behavioral.visitor;

public interface EmployeeVisitor {

    void visitManager(Manager manager);
    void visitFloormen(Floorman floorman);
}
