package behavioral.chainOfResponsibility;

import lombok.Setter;

import java.util.List;
@Setter
public class HelpRequestHolder {

    private ScreenHelpRequestHandler handler;

    public void getHelpForCurrentScreen(String request) {
        handler.handleRequest(request);
    }

}
