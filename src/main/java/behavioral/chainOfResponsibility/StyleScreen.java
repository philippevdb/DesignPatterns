package behavioral.chainOfResponsibility;

public class StyleScreen extends ScreenHelpRequestHandler{

    @Override
    public void handleRequest(String request) {
        if (request.equals("style")) {
            super.getResponse();
        } else {
            super.handleRequest(request);
        }
    }
}
