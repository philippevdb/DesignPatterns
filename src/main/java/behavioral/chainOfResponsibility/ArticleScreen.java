package behavioral.chainOfResponsibility;

public class ArticleScreen extends ScreenHelpRequestHandler {

    @Override
    public void handleRequest(String request) {
        if (request.equals("article")) {
            super.getResponse();
        } else {
            super.handleRequest(request);
        }
    }
}
