package behavioral.chainOfResponsibility;

import java.util.Objects;

public abstract class ScreenHelpRequestHandler {
    private ScreenHelpRequestHandler next;

    public void setNext(ScreenHelpRequestHandler next) {
        this.next = next;
    }
    public void handleRequest(String request) {
        if (next != null) {
            next.handleRequest(request);
        }
    }

    protected void getResponse() {
        System.out.println(this.getClass() + ": gave help");
    }
}
