package behavioral.chainOfResponsibility;

public class MainScreen extends ScreenHelpRequestHandler {

    @Override
    public void handleRequest(String request) {
        if (request.equals("main")) {
            super.getResponse();
        } else {
            super.handleRequest(request);
        }
    }
}
