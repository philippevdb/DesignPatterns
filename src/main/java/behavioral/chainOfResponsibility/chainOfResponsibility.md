good way to avoid nested if-statements or to handle a sequence of processes which have to be handled sequentially.

Use when:
<ul>
    <li>more than one object may handle a request, and the handler isn't known a priori. The handler should be ascertained automatically.</li>
    <li>you want to issue a request to one of several objects without specifying the receiver explicitly.</li>
    <li>the set of objects that can handle a request should be specified dynamically.</li>
</ul>

Consequences:
<ul>
    <li>reduced coupling: frees object from knowing who handles a request</li>
    <li>added flexibility in assigning responsibilities to objects</li>
    <li>receipt is guaranteed</li>
</ul>

Related to: Composite, often applied in conjunction