package behavioral.chainOfResponsibility;

public class ChainOfResponsibility {

    public static void main(String[] args) {
        HelpRequestHolder helpInterface = new HelpRequestHolder();
        ScreenHelpRequestHandler mainScreen = new MainScreen();
        ScreenHelpRequestHandler articleScreen = new ArticleScreen();
        ScreenHelpRequestHandler styleScreen = new StyleScreen();
        mainScreen.setNext(articleScreen);
        articleScreen.setNext(styleScreen);
        helpInterface.setHandler(mainScreen);
        helpInterface.getHelpForCurrentScreen("style");
        helpInterface.getHelpForCurrentScreen("article");
        helpInterface.getHelpForCurrentScreen("main");
    }
}
