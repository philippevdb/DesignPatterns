Use when:
<ul>
    <li>one aspect of an abstraction is dependent on the other</li>
    <li>change to one object requires changing the other</li>
    <li>ability to notify others without making assumptions of who these objects are(loose cooupling)</li>
</ul>

Consequences:
<ul>
    <li>abstract coupling between subject and observer</li>
    <li>support for broadcast communication</li>
    <li>uncexpected updates</li>
</ul>

Related to: 
<ul>
    <li>Mediator: encapsulating complex update semantics</li>
    <li>Singleton: changemanager may use a singleton pattern</li>
</ul>