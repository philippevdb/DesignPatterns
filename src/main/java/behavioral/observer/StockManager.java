package behavioral.observer;

import java.util.ArrayList;
import java.util.List;

public class StockManager {

    private List<StockObserver> observers;

    public void addObserver(StockObserver observer) {
        if (observers == null) {
            observers = new ArrayList<>();
        }
        observers.add(observer);
    }

    public void simulateGoingOutOfStock(String article) {
        System.out.println("Article out of stock: " + article);
        notifyObservers(article);
    }

    private void notifyObservers(String article) {
        if (observers != null) {
            observers.forEach(observer -> observer.notifyOutOfStock(article));
        }
    }
}
