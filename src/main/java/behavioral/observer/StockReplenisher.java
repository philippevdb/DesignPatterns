package behavioral.observer;

import structural.facade.Stock;

public class StockReplenisher implements StockObserver {
    @Override
    public void notifyOutOfStock(String article) {
        if (article.equals("article1")) {
            System.out.println("Replenish stock of " + article);
        }
    }
}
