package behavioral.observer;

public class Observer {

    public static void main(String[] args) {
        StockManager manager = new StockManager();
        manager.addObserver(new StockReplenisher());
        manager.simulateGoingOutOfStock("article1");
        manager.simulateGoingOutOfStock("article2");
    }
}
