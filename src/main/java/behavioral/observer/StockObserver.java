package behavioral.observer;

public interface StockObserver {

    void notifyOutOfStock(String article);
}
