package behavioral.state;

public class OutOfStock implements StockState{
    @Override
    public void reserve(String article) {
        System.out.println("notify article out of stock: " + article);
    }
}
