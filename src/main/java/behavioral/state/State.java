package behavioral.state;

import structural.facade.Stock;

public class State {
    public static void main(String[] args) {
        StockState inStock = new InStock();
        new StockMovementHelper("article", inStock).reserve();
        StockState outOfStock = new OutOfStock();
        new StockMovementHelper("article", outOfStock).reserve();
    }
}
