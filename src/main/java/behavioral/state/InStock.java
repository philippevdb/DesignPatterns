package behavioral.state;

public class InStock implements StockState {
    @Override
    public void reserve(String article) {
        System.out.println("Reserve article stock: " + article);
    }
}
