package behavioral.state;

public interface StockState {

    void reserve(String article);
}
