package behavioral.state;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class StockMovementHelper {

    private final String article;
    private final StockState state;

    void reserve() {
        state.reserve(article);
    }
}
