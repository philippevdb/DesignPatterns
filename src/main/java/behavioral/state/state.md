Use when:
<ul>
    <li>objects behaviour depends on its state and it must change its behaviour at runtime</li>
    <li>operations have large multipart conditionals that depend on the object's state</li>
</ul>

Consequences:
<ul>
    <li>localizes state-specific behaviour and partitions behaviour for different classes.</li>
    <li>makes state transitions explicit</li>
    <li>state objects can be shared</li>
</ul>

Related to:
<ul>
    <li>Flyweight: explains when and state how</li>
    <li>Singleton: often singletons</li>
</ul>