package behavioral.mediator;

import java.util.Map;

public class WarehouseDelivery implements Picking{
    @Override
    public void createPicking(Map<String, Integer> countsByArticle) {
        System.out.println("Deliver to warehouse. " + countsByArticle);
    }
}
