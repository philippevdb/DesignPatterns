package behavioral.mediator;

import java.util.Map;

public class HomeDelivery implements Picking{
    @Override
    public void createPicking(Map<String, Integer> countsByArticle) {
        System.out.println("Delivery articles to consumer home. " + countsByArticle);
    }
}
