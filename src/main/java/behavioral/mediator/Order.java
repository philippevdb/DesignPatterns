package behavioral.mediator;

import java.util.Map;

public interface Order {

    Map<String,Integer> fetchOrdersByArticle();

    void addOrder(String article, int count);
}
