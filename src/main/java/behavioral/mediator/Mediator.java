package behavioral.mediator;

public class Mediator {

    public static void main(String[] args) {
        Order bpOrder = new BusinessPortalOrder();
        bpOrder.addOrder("article1", 20);
        bpOrder.addOrder("article2", 30);
        OrderPickingMediator mediator = new OrderPickingMediator();
        mediator.addOrder(bpOrder);
        mediator.createPicking(PickingType.B2B);

        Order webOrder = new WebshopOrder();
        webOrder.addOrder("article3", 1);
        webOrder.addOrder("article4", 1);
        mediator = new OrderPickingMediator();
        mediator.addOrder(webOrder);
        mediator.createPicking(PickingType.HOME_DELIVERY);
    }
}
