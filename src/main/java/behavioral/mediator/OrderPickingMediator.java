package behavioral.mediator;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@AllArgsConstructor
public class OrderPickingMediator {

    private final List<Order> orders = new ArrayList<>();

    public void createPicking(PickingType pickingType) {
        switch (pickingType) {
            case B2B:
                new WarehouseDelivery().createPicking(fetchOrderQuantities(orders));
                break;
            case HOME_DELIVERY:
                new HomeDelivery().createPicking(fetchOrderQuantities(orders));
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + pickingType);
        }
    }

    private Map<String,Integer> fetchOrderQuantities(List<Order> orders) {
        return orders.stream()
                .flatMap(order -> order.fetchOrdersByArticle().entrySet().stream())
                .collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));
    }

    public void addOrder(Order order) {
        orders.add(order);
    }
}
