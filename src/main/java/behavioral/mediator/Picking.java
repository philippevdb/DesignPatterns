package behavioral.mediator;

import java.util.Map;

public interface Picking {

    void createPicking(Map<String,Integer> countsByArticle);
}
