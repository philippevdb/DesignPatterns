Encapsulating collective behaviour in a separate object, repsonsible for controlling and coordinating the interactions of a group of objects.

Use when: 
<ul>
    <li>communication is well-defined but complex. The resulting interdependencies are unstructured and difficult to understand.</li>
    <li>reusing an object is difficult because it refers to and communicates with many other objects.</li>
    <li>behaviour that's distributed between several classes should be customizable without a lot of subclassing.</li>
</ul>

Consequences:
<ul>
    <li>it limits subclassing</li>
    <li>loose coupling</li>
    <li>simplifies object protocols. Mediator replaces many-to-many interactions with one-to-many interactions between the mediator and its colleagues.</li>
    <li>it abstracts how objects cooperate</li>
    <li>centralizes control</li>
</ul>

Related to:
<ul>
    <li>
        Facade: differs from mediator in that it abstracts a subsystem of objects to provide a more convenient interface.
        Its protocol is unidirectional. IN contrast mediator enables cooperative behavior that colleague objects don't  or can't provide and the protocol is multidirectional.
    </li>
    <li>Observer: colleagues can communicate with mediator using observer</li>
</ul>