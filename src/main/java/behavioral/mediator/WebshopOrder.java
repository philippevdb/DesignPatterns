package behavioral.mediator;

import java.util.ArrayList;
import java.util.Map;

public class WebshopOrder implements Order {

    private Map<String,Integer> ordersByArticle;

    @Override
    public Map<String, Integer> fetchOrdersByArticle() {
        return ordersByArticle;
    }

    @Override
    public void addOrder(String article, int count) {
        if (ordersByArticle == null) {
            new ArrayList<>();
        }
        ordersByArticle.put(article,count);
    }
}
