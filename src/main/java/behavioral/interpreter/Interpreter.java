package behavioral.interpreter;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class Interpreter {

    private static List<String> operations = Arrays.asList("+","-","*");

    public static void main(String[] args) {
        final var tokenString = "4 3 2 - 1 + *";
        var stack = new Stack<Expression>();
        for (var s : tokenString.split(" ")) {
            if (isOperator(s)) {
                var rightExpression = stack.pop();
                var leftExpression = stack.pop();
                System.out.println("popped from stack: left(" + leftExpression.interpret() + ") right(" + rightExpression.interpret() + ")");
                var operator = getOperatorInstance(s, leftExpression, rightExpression);
                System.out.println("operator(" + operator + ")");
                var result = operator.interpret();
                var resultExpression = new NumberExpression(result);
                stack.push(resultExpression);
                System.out.println("push result to stack: " + resultExpression.interpret());
            } else {
                var number = new NumberExpression(s);
                stack.push(number);
                System.out.println("push to stack: " + number.interpret());
            }
        }
        System.out.println("result: " + stack.pop().interpret());
    }

    private static boolean isOperator(String s) {
        return operations.contains(s);
    }

    private static Expression getOperatorInstance(String s, Expression leftExpression, Expression rightExpression) {
        Expression plus = new PlusExpression(leftExpression, rightExpression);
        if (s.equals(plus.toString())) {
            return plus;
        }
        Expression minus = new MinusExpression(leftExpression, rightExpression);
        if (s.equals(minus.toString())) {
            return minus;
        }
        Expression multiply = new MultiplyExpression(leftExpression, rightExpression);
        if (s.equals(multiply.toString())) {
            return multiply;
        }
        return null;
    }
}
