Use when there is a language to interpret and you can represent statements in the language as abstract syntax trees. The interpreter pattern works best when:
<ul>
    <li>Grammar is simple. For complex grammars the hierarchy for the grammar becomes large and unmanageable.</li>
    <li>efficiency is not a critical concern</li>
</ul>

Consequences:
<ul>
    <li>It's easy to change and extend the grammar.</li>
    <li>Implementing the grammar is easy.</li>
    <li>Complex grammars are hard to maintain.</li>
    <li>Makes it easier to evaluate an expression in a new way.</li>
</ul>

Related to: 
<ul>
    <li>Composite: abstract syntax tree is an instance of the composite pattern.</li>
    <li>Flyweight: shows how to share terminal symbols within abstract syntax tree.</li>
    <li>Interpreter: can use iterator to traverse the structure.</li>
    <li>Visitor: can be used to maintain dthe behavior in each node in the abstract syntax tree in one class.</li>
</ul>