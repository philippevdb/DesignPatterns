package behavioral.memento;

import java.util.LinkedList;
import java.util.List;

public interface CareTaker {

    List<Memento> mementos = new LinkedList<>();

    default void addMemento(Memento memento) {
        mementos.add(memento);
    }

    default Memento getMemento(int index) {
        return mementos.get(index);
    }
}
