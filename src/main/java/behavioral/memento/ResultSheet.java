package behavioral.memento;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResultSheet {

    private double totalSales;
    private double totalRevenue;
    private double ebitda;
    private double ebit;
    private double netResult;
}
