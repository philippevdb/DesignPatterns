package behavioral.memento;

public interface Memento<T> {

    T getSnapshot();
}
