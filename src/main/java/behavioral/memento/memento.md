Use when:
<ul>
    <li>snapshot of an object's state must be saved so that it can be restored to that state later.</li>
    <li>direct interface to obtaining the state would expose implementation details and break object's encapsulation.</li>
</ul>

Consequences:
<ul>
    <li>Preserving encapsulation boundaries: shields other objects from potentially complex originator internals.</li>
    <li>simplifies originator</li>
    <li>using mementos might be expensive</li>
    <li>defining narrow and wide interfaces: it may be difficult in some languages to ensure that only the originator can access the mememento's state.</li>
    <li>hidden costs in caring for mementos</li>
</ul>

Relatied to:
<ul>
    <li>Command: commands can use mementos to maintain state for undoable operations</li>
    <li>Iterator: can be used for iteration</li>
</ul>