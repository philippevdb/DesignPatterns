package behavioral.memento;

public class MementoMain {

    public static void main(String[] args) {
        CareTaker careTaker = new ResultCareTaker();
        ResultSheet result = new ResultSheet(50000D,55000D,5000D,4800D,2600D);
        careTaker.addMemento(new ResultMemento(result));
        result = new ResultSheet(51000D,55500D,3000D,2900D,500D);
        careTaker.addMemento(new ResultMemento(result));
        result = new ResultSheet(53000D,57000D,3500D,3200D,2000D);
        careTaker.addMemento(new ResultMemento(result));
        result = new ResultSheet(52500D,56000D,3000D,3200D,2000D);
        careTaker.addMemento(new ResultMemento(result));

        for (int i=0; i<4;i++) {
            Memento<ResultSheet> snapshot = careTaker.getMemento(i);
            System.out.println(i + " - totalSales: " + snapshot.getSnapshot().getTotalSales());
            System.out.println(i + " - totalRevenue: " + snapshot.getSnapshot().getTotalRevenue());
            System.out.println(i + " - ebitda: " + snapshot.getSnapshot().getEbitda());
            System.out.println(i + " - ebit: " + snapshot.getSnapshot().getEbit());
            System.out.println(i + " - netResult: " + snapshot.getSnapshot().getNetResult());
        }
    }
}
