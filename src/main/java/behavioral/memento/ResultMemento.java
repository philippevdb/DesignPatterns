package behavioral.memento;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.xml.transform.Result;

@Data
public class ResultMemento implements Memento<ResultSheet> {

    private double totalSales;
    private double totalRevenue;
    private double ebitda;
    private double ebit;
    private double netResult;

    public ResultMemento(ResultSheet result) {
        this.totalSales = result.getTotalSales();
        this.totalRevenue = result.getTotalRevenue();
        this.ebitda = result.getEbitda();
        this.ebit = result.getEbit();
        this.netResult = result.getNetResult();
    }

    @Override
    public ResultSheet getSnapshot() {
        return new ResultSheet(totalSales,totalRevenue,ebitda, ebit, netResult);
    }
}
