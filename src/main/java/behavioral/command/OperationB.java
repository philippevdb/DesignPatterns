package behavioral.command;

public class OperationB implements Command {
    @Override
    public void execute() {
        System.out.println("execute operation B");
    }

    @Override
    public void revert() {
        System.out.println("revert operation B");
    }
}
