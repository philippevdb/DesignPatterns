package behavioral.command;

import java.util.Deque;
import java.util.LinkedList;

public class OperationHolder {

    private final Deque<Command> history = new LinkedList<>();

    public void doOperation(Command command) {
        command.execute();
        history.add(command);
    }

    public void undoLastOperation() {
        if (history.size() > 0) {
            Command command = history.removeLast();
            command.revert();
        }
    }

    public void redoLastOperation() {
        Command command = history.pollLast();
        if (command != null) {
            doOperation(command);
        }
    }
}
