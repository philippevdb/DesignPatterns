package behavioral.command;

public class OperationA implements Command {
    @Override
    public void execute() {
        System.out.println("execute operation A");
    }

    @Override
    public void revert() {
        System.out.println("revert operation A");
    }
}
