Use when:
<ul>
    <li>parameterize objects by action to perform. Can be used for callbacks.</li>
    <li>specify, queue and execute requests at different times</li>
    <li>support undo</li>
    <li>support logging changes so they can be reapplied in case of a system crash.</li>
    <li>structure system around high-level operations built on primitives operations. <u>The command pattern offers a way to model transactions.</u></li>
</ul>

Consequences:
<ul>
    <li>command decouples object that invokes operation from the one that knows how to perform it</li>
    <li>commands can be manipulated and extended like any other object</li>
    <li>you can assemble commands into a composite command. For example: macro command</li>
    <li>easy to add new commands</li>
</ul>

Related to:
<ul>
    <li>Composite: can be used to implement MacroCommands</li>
    <li>Memento: can keep state the command requires to undo effect</li>
    <li>Prototype: command that must be copied before being placed on history acts as prototype</li>
</ul>