package behavioral.command;

public class CommandMain {

    public static void main(String[] args) {
        OperationHolder operationHolder = new OperationHolder();
        operationHolder.doOperation(new OperationA());
        operationHolder.doOperation(new OperationB());
        operationHolder.undoLastOperation();
        operationHolder.redoLastOperation();
        operationHolder.undoLastOperation();
    }
}
