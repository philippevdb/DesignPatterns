package behavioral.rules;

import lombok.Data;

@Data
public class Ticket {

    private double value;
}
