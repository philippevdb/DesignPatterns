use when:
<ul>
    <li>replacing nested ifs</li>
    <li>working with a lot of conditionals</li>
</ul>

How:
<ol>
    <li>extract method refactor and give descriptive names</li>
    <li>create interface where you process a given action</li>
    <li>implement interface with rules where you evaluate the condition and act upon it accordingly. For this you can split the conditional and execution up into two methods or combine them in one.</li>
    <li>create class that adds rules to list and then runs through them</li>
</ol>

related to:
<ul>
    <li>strategy pattern: every rule and its execution is a strategy in a way</li>
    <li>factory pattern: running through the rules, is like a factory pattern</li>
</ul>