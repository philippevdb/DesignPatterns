package behavioral.rules;

public interface IPromotionRule {

    boolean isApplicable(Ticket ticket);

    void execute(Ticket ticket);
}
