package behavioral.rules;

public class Rules {

    public static void main(String[] args) {
        Ticket ticket = new Ticket();
        ticket.setValue(40D);
        PromotionEvaluator promotions = new PromotionEvaluator();
        promotions.evaluate(ticket);
        System.out.println(ticket.getValue());
    }
}
