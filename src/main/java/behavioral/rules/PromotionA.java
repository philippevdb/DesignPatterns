package behavioral.rules;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class PromotionA implements IPromotionRule {

    @Override
    public boolean isApplicable(Ticket ticket) {
        return true;
    }

    @Override
    public void execute(Ticket ticket) {
        ticket.setValue(ticket.getValue() * 0.9D);
    }
}
