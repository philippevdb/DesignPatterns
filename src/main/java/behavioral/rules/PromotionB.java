package behavioral.rules;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class PromotionB implements IPromotionRule {

    @Override
    public boolean isApplicable(Ticket ticket) {
        return ticket.getValue() > 50D;
    }

    @Override
    public void execute(Ticket ticket) {
        ticket.setValue(ticket.getValue() - 2.5D);
    }
}
