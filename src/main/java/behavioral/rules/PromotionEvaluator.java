package behavioral.rules;

import java.util.ArrayList;
import java.util.List;

public class PromotionEvaluator {

    private final List<IPromotionRule> rules;

    public PromotionEvaluator() {
        this.rules = new ArrayList<>();
        rules.add(new PromotionA());
        rules.add(new PromotionB());
    }

    public void evaluate(Ticket ticket) {
        rules.stream()
                .filter(rule -> rule.isApplicable(ticket))
                .forEach(rule -> rule.execute(ticket));
    }
}
