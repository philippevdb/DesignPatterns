package behavioral.iterator;

public class IteratorPattern {

    public static void main(String[] args) {
        ImageRepo imageRepo = new ImageRepo();
        ImageIterator iterator = imageRepo.iterator(ImageType.EVEN);
        while(iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
