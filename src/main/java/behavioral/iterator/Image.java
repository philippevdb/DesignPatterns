package behavioral.iterator;

import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@Getter
public class Image {

    private final String name;

    @Override
    public String toString() {
        return name;
    }
}
