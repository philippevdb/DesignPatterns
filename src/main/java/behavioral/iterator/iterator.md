Use when:
<ul>
    <li>To access an aggregate object's contents without exposing its internal representation.</li>
    <li>to support multiple traversals of aggregate objects.</li>
    <li>to provide a uniform interface for traversing different aggregate structures that is, to support polymorphic iteration.</li>
</ul>

Consequences:
<ul>
    <li>supports variations in the traversal of an aggregate</li>
    <li>iterators simplefy the aggregate interface</li>
    <li>more than one traversal can be pending on an aggregate</li>
</ul>

Related to:
<ul>
    <li>Composite: often applied to recursive structures such as composites.</li>
    <li>Factory method: rely on factory methods to instantiate the appropriate iterator subclass.</li>
    <li>often used in conjunction. An iterator can use a memento to capture the state of an iteration.</li>
</ul>