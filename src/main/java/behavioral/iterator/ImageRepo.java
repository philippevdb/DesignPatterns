package behavioral.iterator;

import java.util.ArrayList;
import java.util.List;

public class ImageRepo {

    private List<Image> images;

    public ImageRepo() {
        images = new ArrayList<>();
        images.add(new Image("1"));
        images.add(new Image("2"));
        images.add(new Image("3"));
        images.add(new Image("4"));
        images.add(new Image("5"));
        images.add(new Image("6"));
        images.add(new Image("7"));
        images.add(new Image("8"));
    }

    public ImageIterator iterator(ImageType imageType) {
        return new ImageIterator(images, imageType);
    }

}
