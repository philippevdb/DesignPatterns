package behavioral.iterator;

import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

public class ImageIterator implements Iterator<Image> {

    private final ImageType imageType;
    private final List<Image> images;
    private int index = 0;

    public ImageIterator(List<Image> images, ImageType imageType) {
        this.imageType = imageType;
        this.images = images.stream()
                .filter(image ->
                        ImageType.EVEN.equals(imageType) ?
                                Integer.valueOf(image.getName()) % 2 == 0
                                : Integer.valueOf(image.getName()) % 2 == 1)
                .collect(Collectors.toList());
    }

    @Override
    public boolean hasNext() {
        return !images.isEmpty() && images.size() > index;
    }

    @Override
    public Image next() {
        return images.get(index++);
    }
}
