package behavioral.strategy;

public interface StockMovementProtocol {

    void moveStock(String article, int quantity);
}
