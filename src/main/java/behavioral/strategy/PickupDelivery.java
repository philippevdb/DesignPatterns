package behavioral.strategy;

public class PickupDelivery implements StockMovementProtocol{
    @Override
    public void moveStock(String article, int quantity) {
        System.out.println("Prepare for pickup: " + quantity + " " + article);
    }
}
