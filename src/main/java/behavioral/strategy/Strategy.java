package behavioral.strategy;

public class Strategy {
    public static void main(String[] args) {
        StockMovementHelper helper = new StockMovementHelper();
        helper.setProtocol(new ConsumerDelivery());
        helper.moveStock("article1", 1);
        helper.setProtocol(new PickupDelivery());
        helper.moveStock("article2", 1);
    }
}
