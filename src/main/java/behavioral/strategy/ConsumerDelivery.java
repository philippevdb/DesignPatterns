package behavioral.strategy;

public class ConsumerDelivery implements StockMovementProtocol{
    @Override
    public void moveStock(String article, int quantity) {
        System.out.println("Send to consumer: " + quantity + " " + article);
    }
}
