package behavioral.strategy;

import lombok.Data;

@Data
public class StockMovementHelper {

    private StockMovementProtocol protocol;

    void moveStock(String article, int quantity) {
        if (protocol == null) {
            throw new NullPointerException("Protocol not set");
        }
        protocol.moveStock(article, quantity);
    }
}
