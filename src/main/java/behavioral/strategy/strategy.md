Use when:
<ul>
    <li>many related classes differ only in their behavior. Strategies provide a way to configure a class with one of many behaviours.</li>
    <li>you need different variants of an algorithm.</li>
    <li>algorithm uses data that clients shouldn't know about.</li>
    <li>class defines many behaviours and these appear as multiple conditional statements.</li>
</ul>

Consequences:
<ul>
    <li>families of related algorithms</li>
    <li>alternative to subclassing, which hard-wires behaviour into context.</li>
    <li>strategies eliminate conditional statements</li>
    <li>a choice of implementations</li>
    <li>clients must be aware of different strategies</li>
    <li>communication overhead between strategy and context</li>
    <li>increased number of objects</li>
</ul>

Related to: Flyweight: strategy objects make good flyweights