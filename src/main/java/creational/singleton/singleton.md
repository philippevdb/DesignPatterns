Use when:
<ul>
    <li>there must be exactly one instance of a class and it must be accessible to clients from a well-known access point</li>
    <li>when the sole instance should be extensible by subclassing and clients should be able to use an extended instance without modifying their code</li>
</ul>