package creational.singleton;

public class Singleton {

    private static SomeContext context;

    public static SomeContext getContext() {
        if (context == null) {
            context = new SomeContext();
        }
        return context;
    }

    public static void main(String[] args) {
        System.out.println(getContext().equals(getContext()));
    }

}
