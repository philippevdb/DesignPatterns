package creational.factoryMethod;

public class Stewardess implements Employee {
    @Override
    public Uniform buyUniform() {
        return new StewardessUniform();
    }
}
