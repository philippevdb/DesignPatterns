package creational.factoryMethod;

public class Pilot implements Employee {
    @Override
    public Uniform buyUniform() {
        return new PilotUniform();
    }
}
