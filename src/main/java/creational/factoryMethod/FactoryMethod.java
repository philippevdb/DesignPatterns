package creational.factoryMethod;

public class FactoryMethod {

    public static void main(String[] args) {
        Employee pilot = new Pilot();
        System.out.println(pilot.buyUniform());
        Employee stewardess = new Stewardess();
        System.out.println(stewardess.buyUniform());
    }
}
