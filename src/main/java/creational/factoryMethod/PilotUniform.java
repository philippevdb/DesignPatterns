package creational.factoryMethod;

public class PilotUniform implements Uniform {

    @Override
    public String toString() {
        return "PilotUniform";
    }
}
