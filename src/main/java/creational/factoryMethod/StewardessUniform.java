package creational.factoryMethod;

public class StewardessUniform implements Uniform {

    @Override
    public String toString() {
        return "StewardessUniform";
    }
}
