Use when:
<ul>
    <li>class can't anticipate the class of objects it must create</li>
    <li>class wants its subclasses to specify the objects it creates</li>
    <li>classes delegate responsability to one of several helper subclasses and you want to localize the knowledge of which helper subclass is the delegate</li>
</ul>

Consequences:
<ul>
    <li>provides hooks for subclasses</li>
    <li>connects parallel class hierarchies</li>
</ul>

Relates to:
<ul>
    <li>Abstract factory: often implemented with factory methods. Factory methods don't require a parameter and don't have to belong to the same family.</li>
    <li>Template method: usually called within template methods. </li>
    <li>Prototype: doesn't require subclassing Creator. However, they often require an initialize operation on the Product class. Creator uses Initialize to initialize an object. Factory method doesn't require such an operation.</li>
</ul>