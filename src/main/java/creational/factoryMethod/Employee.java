package creational.factoryMethod;

public interface Employee {

    Uniform buyUniform();
}
