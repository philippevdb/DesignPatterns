package creational.abstractFactory;

public class WarningIcon implements Icon {

    @Override
    public void showIcon() {
        System.out.println("Exclamation mark icon.");
    }
}
