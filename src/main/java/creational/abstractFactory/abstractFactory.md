Use when:
<ul>
    <li>system should be independent of how its products are created, composed and represented.</li>
    <li>configured with one of <u>multiple</u> families of products</li>
    <li>family of related product objects is designed to be used together and you need to inforce this constraint</li>
    <li>you want to provide interfaces and not their implementations</li>
</ul>

consequences:
<ul>
    <li>isolates concrete classes: helps control the classes of objects that an application creates because a factory encapsulates responsability. It isolates clients from implementation clients.</li>
    <li>makes exchanging product families easy</li>
    <li>promotes consistency among product</li>
    <li>supporting new kinds of products is difficult</li>
</ul>

related to:
<ul>
    <li>Often implemented with Factory method, but could also be done with prototype</li>
    <li>often a singleton</li>
</ul>