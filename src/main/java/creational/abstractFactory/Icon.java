package creational.abstractFactory;

public interface Icon {

    void showIcon();
}
