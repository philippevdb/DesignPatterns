package creational.abstractFactory;

public interface DialogBar {

    void provideTitle();
}
