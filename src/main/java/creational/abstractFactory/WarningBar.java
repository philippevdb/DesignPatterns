package creational.abstractFactory;

public class WarningBar implements DialogBar {
    @Override
    public void provideTitle() {
        System.out.println("Warning");
    }
}
