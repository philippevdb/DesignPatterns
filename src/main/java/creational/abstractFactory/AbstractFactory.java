package creational.abstractFactory;

public class AbstractFactory {

    public static void main(String[] args) {
        Dialog errorDialog = DialogFactory.get(DialogFactory.DialogType.ERROR, "message 1");
        errorDialog.createErrorBar().provideTitle();
        errorDialog.createIcon().showIcon();
        System.out.println(errorDialog.showMessage());
        Dialog warningDialog = DialogFactory.get(DialogFactory.DialogType.WARNING, "message 2");
        warningDialog.createErrorBar().provideTitle();
        warningDialog.createIcon().showIcon();
        System.out.println(warningDialog.showMessage());
    }
}
