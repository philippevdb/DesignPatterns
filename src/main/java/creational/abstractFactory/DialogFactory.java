package creational.abstractFactory;

public class DialogFactory {

    public enum DialogType {
        WARNING,ERROR
    }

    public static Dialog get(DialogType type, String message) {
        switch(type) {
            case WARNING: return new WarningDialog(message);
            case ERROR: return new ErrorDialog(message);
            default: throw new IllegalArgumentException("DialogType doesn't exist: " + type);
        }
    }
}
