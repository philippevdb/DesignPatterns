package creational.abstractFactory;

public class ErrorIcon implements Icon {
    @Override
    public void showIcon() {
        System.out.println("Big red X icon.");
    }
}
