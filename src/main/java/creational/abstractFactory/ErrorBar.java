package creational.abstractFactory;

public class ErrorBar implements DialogBar {

    @Override
    public void provideTitle() {
        System.out.println("ERROR");
    }
}
