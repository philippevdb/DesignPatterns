package creational.abstractFactory;

public class WarningDialog extends Dialog {

    public WarningDialog(String message) {
        this.message = message;
    }

    @Override
    public DialogBar createErrorBar() {
        return new WarningBar();
    }

    @Override
    public Icon createIcon() {
        return new WarningIcon();
    }

}
