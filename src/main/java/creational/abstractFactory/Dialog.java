package creational.abstractFactory;

public abstract class Dialog {

    protected String message;

    abstract DialogBar createErrorBar();
    abstract Icon createIcon();
    public String showMessage() {
        return message;
    }

}
