package creational.abstractFactory;

public class ErrorDialog extends Dialog {

    public ErrorDialog(String message) {
        this.message = message;
    }

    @Override
    public DialogBar createErrorBar() {
        return new ErrorBar();
    }

    @Override
    public Icon createIcon() {
        return new ErrorIcon();
    }

}
