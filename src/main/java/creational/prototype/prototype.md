Use when:
<ul>
    <li>classes to instantiate are specified at runtime</li>
    <li>avoid building a class hierarchy of factories that parallels the class hierarchy of products</li>
    <li>instances of a class can have one of only a few different combinations of state. It maybe more convenient to install a corresponding number of prototypes to clone them rather than instantiating the class manually.</li>
</ul>

Consequences:
<ul>
    <li>adding and removing products at runtime</li>
    <li>specifying new objects by varying values</li>
    <li>specifying new objects by varying structure</li>
    <li>reduced subclassing</li>
    <li>configuring an application with classes dynamically</li>
</ul>