package creational.prototype;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class CharacterFactory {

    private final Soldier soldier;
    private final Officer officer;

    public Soldier createSoldier() {
        return (Soldier) soldier.clone();
    }

    public Officer createOfficer() {
        return (Officer) officer.clone();
    }
}
