package creational.prototype;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Officer extends Character {
    public Officer(Officer officer) {
        super(officer);
    }
}
