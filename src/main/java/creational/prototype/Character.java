package creational.prototype;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public abstract class Character extends CharacterPrototype<Character> {

    public Character(Character character) {
    }


    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
