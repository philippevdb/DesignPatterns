package creational.prototype;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Soldier extends Character {
    public Soldier(Soldier soldier) {
        super(soldier);
    }
}
