package creational.prototype;

public class Prototype {

    public static void main(String[] args) {
        Soldier originalSoldier = new Soldier();
        Officer originalOfficer = new Officer();
        CharacterFactory factory = new CharacterFactory(originalSoldier, originalOfficer);
        Soldier cloneSoldier = factory.createSoldier();
        System.out.println(cloneSoldier.getClass().getSimpleName() + ": " + originalOfficer.equals(cloneSoldier));
        Officer cloneOfficer = factory.createOfficer();
        System.out.println(cloneOfficer.getClass().getSimpleName() + ": " + originalOfficer.equals(cloneOfficer));
    }
}
