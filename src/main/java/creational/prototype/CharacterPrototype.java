package creational.prototype;

import lombok.SneakyThrows;

public abstract class CharacterPrototype<T> implements Cloneable {

    @SneakyThrows
    @Override
    public T clone() {
        return (T) this.clone();
    }
}
