Use when:
<ul>
    <li>algorithm to create object should be independent of the parts that make up the object and how they're assembled.</li>
    <li>the construction process must allow different representations for the object that's constructed.</li>
</ul>

Consequences:
<ul>
    <li>it lets you vary a product's internal representation</li>
    <li>it isolates code for construction and representation</li>
    <li>it gives you finer control over the consturction process</li>
</ul>

Related to:
<ul>
    <li>Abstract factory: both are concerned with the construction of complex objects. Builders focus on building step by step and only returning at the end. Factory returns immediately.</li>
    <li>Composite: is what the builder often builds.</li>
</ul>