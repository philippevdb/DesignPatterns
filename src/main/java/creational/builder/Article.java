package creational.builder;

public class Article {

    private String colour;
    private String description;
    private String washingInstructions;
    private String size;
    private String brand;
    private String quality;

    public Article(String colour, String description, String washingInstructions, String size, String brand, String quality) {
        this.colour = colour;
        this.description = description;
        this.washingInstructions = washingInstructions;
        this.size = size;
        this.brand = brand;
        this.quality = quality;
    }

    public static class ArticleBuilder {
        private String colour;
        private String description;
        private String washingInstructions;
        private String size;
        private String brand;
        private String quality;

        public ArticleBuilder colour(String colour) {
            this.colour = colour;
            return this;
        }

        public ArticleBuilder description(String description) {
            this.description = description;
            return this;
        }

        public ArticleBuilder washingInstructions(String washingInstructions) {
            this.washingInstructions = washingInstructions;
            return this;
        }

        public ArticleBuilder size(String size) {
            this.size = size;
            return this;
        }

        public ArticleBuilder brand(String brand) {
            this.brand = brand;
            return this;
        }

        public ArticleBuilder quality(String quality) {
            this.quality = quality;
            return this;
        }

        public Article build() {
            return new Article(colour, description, washingInstructions, size, brand, quality);
        }

    }
}
