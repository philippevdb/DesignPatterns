package creational.builder;

public class Builder {

    public static void main(String[] args) {
        Article article =  new Article.ArticleBuilder()
                .colour("blue")
                .brand("XYZ")
                .description("Blue sweater")
                .quality("100% Cotton")
                .washingInstructions("50 degrees celsius")
                .size("medium")
                .build();
    }
}
