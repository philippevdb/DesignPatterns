package structural.bridge;

public class Human implements Race {

    private Weapon weapon;

    public Human(Weapon weapon) {
        this.weapon = weapon;
    }

    @Override
    public void hit() {
        weapon.attack();
    }

    @Override
    public void protect() {
        weapon.defend();
    }
}
