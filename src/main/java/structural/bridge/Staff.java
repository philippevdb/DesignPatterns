package structural.bridge;

public class Staff implements Weapon {

    @Override
    public void attack() {
        System.out.println("Shoot thunder");
    }

    @Override
    public void defend() {
        System.out.println("Raise shield");
    }
}
