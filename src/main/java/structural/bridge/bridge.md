Use a bridge when:
<ul>
    <li>You want to avoid permanent binding between abstraction and its implementation.</li>
    <li>Both abstractions and implementations should be extensible by subclassing.</li>
    <li>You want to hide the implementations of an abstraction.</li>
    <li>You have a proliferation of classes.</li>
    <li>You want to share an implementation among multiple objects.</li>
</ul>

Consequences:
<ul>
    <li>decoupling interface and implmentation</li>
    <li>improved extensibility</li>
    <li>hiding implementations from clients</li>
</ul>

Related to:
<ul>
    <li>Abstract Factory: can create and configure bridges</li>
    <li>Adapter: is geared towars making unrelated classes work together, usually systems after they've been designed. Bridges are usually used up-front in design.</li>
</ul>