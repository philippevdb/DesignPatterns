package structural.bridge;

public class Elf implements Race {

    private Weapon weapon;

    public Elf(Weapon weapon) {
        this.weapon = weapon;
    }

    @Override
    public void hit() {
        weapon.attack();
    }

    @Override
    public void protect() {
        weapon.defend();
    }
}
