package structural.bridge;

public interface Weapon {

    void attack();
    void defend();
}
