package structural.bridge;

public class Sword implements Weapon {

    public void attack(){
        System.out.println("Stab");
    }

    public void defend(){
        System.out.println("Parry");
    }
}
