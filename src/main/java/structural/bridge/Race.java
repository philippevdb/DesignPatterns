package structural.bridge;

public interface Race {

    void hit();
    void protect();
}
