package structural.facade;

import java.util.List;

public class StockRules {

    private List<String> configurations;

    public List<String> getConfigurations() {
        return configurations;
    }

    public void setConfigurations(List<String> configurations) {
        this.configurations = configurations;
    }
}
