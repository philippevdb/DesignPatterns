package structural.facade;

public class StockDesignator {

    void designateStock() {
        System.out.println("Designate stock from other location according to complex set of rules.");
    }
}
