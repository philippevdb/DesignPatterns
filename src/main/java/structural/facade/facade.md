Can be implemented through interitance and composition.
Keep in mind 'composition over inheritance'.

Use when:
<ul>
    <li>You want to provide a simple interface to a complex subsystem.</li>
    <li>There are is too much interdependency between two subsystems or subsystem and client.</li>
    <li>You want to layer your system.</li>
</ul>

consequences:
<ul>
    <li>Shields subssystem components</li>
    <li>Promotes weak coupling.</li>
    <li>Doesn't prevent applications from using subsystem classes if they need to.</li>
</ul>

Relates to:
<ul>
    <li>Abstract Factory: can be used to provide interface to create subsystem objects</li>
    <li>Mediator: similar in that it abstracts existing classes. A facade abstracts the interface, whereas a mediator abstracts the communication.</li>
    <li></li>
</ul>


