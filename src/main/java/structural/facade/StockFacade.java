package structural.facade;

import java.util.List;

public class StockFacade {

    private List<Stock> stocks;
    private StockDesignator designator;
    private StockRules stockRules;

    public StockFacade() {

    }

    public void moveStock (String from, String to, long quantity) {
        if (stockRules.getConfigurations().equals("some configuration")) {
            designator.designateStock();
        }
    }

    public long getStock(String relation) {
        // fetch correct stocks
        return stocks.stream().filter(stock -> stock.getRelation().equals(relation)).mapToLong(Stock::getStockCount).sum();
    }

}
