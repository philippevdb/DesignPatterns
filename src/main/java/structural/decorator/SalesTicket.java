package structural.decorator;

public interface SalesTicket {

    double getPrice();

    String getMessage();
}
