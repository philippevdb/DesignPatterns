package structural.decorator;

public class FlatSalesDiscount extends SalesTicketDecorator {

    public FlatSalesDiscount(SalesTicket salesTicket) {
        super(salesTicket);
    }

    @Override
    public double getPrice() {
        return salesTicket.getPrice() - 20D; // imagine a discount being added
    }

    @Override
    public String getMessage() {
        return salesTicket.getMessage() + "Promotional discount A of 20 euro has been applied. ";
    }
}
