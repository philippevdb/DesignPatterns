package structural.decorator;

public class OrdinarySalesTicket implements SalesTicket {


    @Override
    public double getPrice() {
        return 50D; // representing sale of articles
    }

    @Override
    public String getMessage() {
        return "";
    }
}
