package structural.decorator;

public class TradeSalesTicket implements SalesTicket {
    @Override
    public double getPrice() {
        return 0D;
    }

    @Override
    public String getMessage() {
        return "Article X exchanged for Article Y from ticket Z.";
    }
}
