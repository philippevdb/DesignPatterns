package structural.decorator;

public class PercentageSalesDiscount extends SalesTicketDecorator {

    public PercentageSalesDiscount(SalesTicket salesTicket) {
        super(salesTicket);
    }

    @Override
    public double getPrice() {
        return salesTicket.getPrice() * 0.95D;
    }

    @Override
    public String getMessage() {
        return salesTicket.getMessage() + "Promotional discount B of 5% has been applied. ";
    }
}
