Use when:
<ul>
    <li>to add responsibilities to individual objects dynamically and transparently, without affecting other objects. So opposite to containing lists, decorators can change behaviour.</li>
    <li>responsabilities can be withdrawn</li>
    <li>when extension by subclassing is impractical. Sometimes independent extensions produce an explosion of subclasses.</li>
</ul>