package structural.decorator;

public class Decorator {

    public static void main(String[] args) {
        SalesTicket ordinarySalesTicket = new OrdinarySalesTicket();
        ordinarySalesTicket = new FlatSalesDiscount(ordinarySalesTicket);
        ordinarySalesTicket = new PercentageSalesDiscount(ordinarySalesTicket);
        System.out.println("price: " + ordinarySalesTicket.getPrice());
        System.out.println(ordinarySalesTicket.getMessage());

        SalesTicket tradeSalesTicket = new TradeSalesTicket();
        System.out.println("price: " + tradeSalesTicket.getPrice());
        System.out.println(tradeSalesTicket.getMessage());
    }
}
