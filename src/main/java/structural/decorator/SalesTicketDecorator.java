package structural.decorator;

public abstract class SalesTicketDecorator implements SalesTicket {

    protected SalesTicket salesTicket;

    public SalesTicketDecorator(SalesTicket salesTicket) {
        this.salesTicket = salesTicket;
    }

}
