package structural.flyweight;

public class Pants extends Article {
    public Pants(String distributionCenter) {
        super(distributionCenter);
        this.setCost(0.5D);
    }
}
