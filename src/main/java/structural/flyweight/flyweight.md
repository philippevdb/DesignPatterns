flyweight: sharing objects without a prohibitive cost of memory or performance
<ul>
    <li>intrinsic state: stored in flyweight. Consists of info independent of flyweight's context and thus shareable.</li>
    <li>extrinsic state: depends on and varies with flyweight's context and therefor can't be shared.</li>
</ul>

Use when all the following are true:
<ul>
    <li>Application uses large number of objects</li>
    <li>Storage costs are high because of the sheer quantity of objects</li>
    <li>Most of the objects state can be made extrinsic</li>
    <li>Many groups of objects may be replaced by relatively few shared objects once extrinsic state is removed</li>
    <li>Application doesn't depend on object identity. Since flyweight objects may be shared, identity tests will return true for conceptually distinct objects.</li>
</ul>

related to:
<ul>
    <li>Composite: often combined to create hierarchical structure with shared leaf nodes</li>
    <li>State and Strategy: often best implemented with flyweights</li>
</ul>