package structural.flyweight;

public class ArticleFactory {

    public static Article create(ArticleType type) {
        switch(type) {
            case TSHIRT:
                return new Tshirt("Location A");
            case COAT:
                return new Coat("Location A");
            case TROUSERS:
                return new Pants("Location B");
            default:
                return null;
        }
    }

    public enum ArticleType {
        TSHIRT, COAT, TROUSERS
    }
}
