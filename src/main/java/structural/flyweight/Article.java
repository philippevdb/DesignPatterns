package structural.flyweight;

public abstract class Article {

    private String distributionCenter; // intrinsic
    private double cost; // extrinsic

    public Article(String distributionCenter) {
        this.distributionCenter = distributionCenter;
    }

    public String getDistributionCenter() {
        return distributionCenter;
    }

    public void setDistributionCenter(String distributionCenter) {
        this.distributionCenter = distributionCenter;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
}
