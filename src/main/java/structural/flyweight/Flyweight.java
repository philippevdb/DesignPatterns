package structural.flyweight;

public class Flyweight {

    public static void main(String[] args) {
        Order order = new Order();
        order.addArticle(ArticleFactory.ArticleType.TSHIRT);
        order.addArticle(ArticleFactory.ArticleType.TSHIRT);
        order.addArticle(ArticleFactory.ArticleType.COAT);
        order.addArticle(ArticleFactory.ArticleType.TROUSERS);
        System.out.println("cost: € " + order.getTotalCost());
        System.out.println("locations: " + order.getLocations());
    }
}
