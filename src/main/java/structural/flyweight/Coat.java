package structural.flyweight;

public class Coat extends Article {
    public Coat(String distributionCenter) {
        super(distributionCenter);
        this.setCost(0.6D);
    }
}
