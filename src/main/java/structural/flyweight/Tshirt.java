package structural.flyweight;

public class Tshirt extends Article {
    public Tshirt(String distributionCenter) {
        super(distributionCenter);
        this.setCost(25D);
    }
}
