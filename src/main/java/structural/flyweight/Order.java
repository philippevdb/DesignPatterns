package structural.flyweight;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Order {

    private List<Article> articles;

    public Order() {
        articles = new ArrayList<>();
    }

    public void addArticle(ArticleFactory.ArticleType type) {
        Article article = ArticleFactory.create(type);
        if (article != null) {
            articles.add(article);
        }
    }

    public double getTotalCost() {
        return articles.stream().mapToDouble(Article::getCost).sum();
    }

    public Map<String,List<Article>> getLocations() {
        return articles.stream().collect(Collectors.groupingBy(Article::getDistributionCenter));
    }
}
