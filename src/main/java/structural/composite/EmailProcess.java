package structural.composite;

import java.util.Arrays;

public class EmailProcess {

    public static void main(String[] args) {
        Team team1 = new Team(Arrays.asList(
           new Employee("employee1"),
           new Employee("employee2"),
           new Employee("employee3")
        ));
        Team team2 = new Team(Arrays.asList(
           new Employee("employee4"),
           new Employee("employee5"),
           new Employee("employee6")
        ));
        Team team3 = new Team(Arrays.asList(
           new Employee("employee7"),
           new Employee("employee8"),
           new Employee("employee9")
        ));
        Team team4 = new Team(Arrays.asList(
           new Employee("employee10"),
           new Employee("employee11"),
           new Employee("employee12")
        ));

        Department department1 = new Department(Arrays.asList(team1, team2));
        Department department2 = new Department(Arrays.asList(team3, team4));

        Company company = new Company(Arrays.asList(department1, department2));

        company.sendEmail("Hello, Company!");
    }
}
