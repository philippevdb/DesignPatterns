package structural.composite;

import java.util.List;

public class Company extends EmployeeComposite {

    public Company(List<Department> departments) {
        departments.forEach(this::add);
    }
}
