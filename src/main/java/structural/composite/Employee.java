package structural.composite;

public class Employee extends EmployeeComposite {

    private final String name;

    public Employee(String name) {
        this.name = name;
    }

    @Override
    public void sendEmail(String message) {
        System.out.println("To: " + name + ", Message: " + message);
    }
}
