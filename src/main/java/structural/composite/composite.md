Describes how to use recursive composition to represent hierarchies in tree-like structures. 
This allows you to treat a group of users as a single one.

Use when:
<ul>
    <li>You want to represent part-whole structures(tree) of objects.</li>
    <li>You want clients to ignore differences between compositions and individual objects and treat all uniformly.</li>
</ul>

Consequences:
<ul>
    <li>Can define hierarchies of simple and composite objects.</li>
    <li>Treat composite structures and individuals uniformly. This simplifies client code.</li>
    <li>Makes it easy to add new types of components. As new components are auto integrated with existing structure.</li>
    <li>Can make your design overly general.</li>
</ul>

Related to:
<ul>
    <li>component-parent link often used for Chain of Responsability.</li>
    <li>Decorators complement Composite.</li>
    <li>Flyweight lets you share components but they can no longer refer to their parents.</li>
    <li>Iterator can be used to traverse composites.</li>
    <li>Visitor localizes operations and behavior that would otherwise be distributed across composite and leaf.</li>
</ul>