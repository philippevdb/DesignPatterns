package structural.composite;

import java.util.ArrayList;
import java.util.List;

public abstract class EmployeeComposite {

    private final List<EmployeeComposite> employees = new ArrayList<>();

    public void add(EmployeeComposite employee) {
        employees.add(employee);
    }

    public void remove(EmployeeComposite employee) {
        employees.remove(employee);
    }

    public EmployeeComposite getEmployee(int sequenceNr) {
        return sequenceNr >= employees.size() ? null : employees.get(sequenceNr);
    }

    public int count() {
        return employees.size();
    }

    public void sendEmail(String message) {
        employees.forEach(employee -> employee.sendEmail(message));
    }
}
