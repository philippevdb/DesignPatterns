package structural.composite;

import java.util.List;

public class Team extends EmployeeComposite {

    public Team(List<Employee> employees) {
        employees.forEach(this::add);
    }
}
