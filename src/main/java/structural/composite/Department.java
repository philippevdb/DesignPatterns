package structural.composite;

import java.util.List;

public class Department extends EmployeeComposite {

    public Department(List<Team> teams) {
        teams.forEach(this::add);
    }
}
