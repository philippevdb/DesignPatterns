package structural.proxy;

public class OrderProxy implements Order {

    private Order order;

    public OrderProxy(Order order) {
        this.order = order;
    }

    @Override
    public Order copy() {
        System.out.println("Proxy serves as stand-in/pointer until proven dat we really need to go through complex process");
        return this;
    }

    @Override
    public String getAnything() {
        Order newOrder = order.copy();
        System.out.println("save new real order to DB");
        return newOrder.getAnything();
    }
}
