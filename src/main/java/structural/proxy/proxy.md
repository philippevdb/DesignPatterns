proxy: create a stand-in object to pretend so you don't have to immediately instantiate an expensive object.

use when: a more versatile and sophisticated reference than a simple pointer is necessary
Examples:
<ul>
    <li>remote proxy: local representative of remote object</li>
    <li>virtual proxy: create virtual proxies on demand(like images)</li>
    <li>protection proxy: control access to the original object. Useful when different objects have different access rights.</li>
    <li>smart reference: performs actions when object is accessed</li>
</ul>

consequences:
<ul>
    <li>remote proxy: can hide that object hides in a different address.</li>
    <li>virtual proxy: can perform optimizations</li>
    <li>protection proxies and smart references: allow additional housekeeping when object is accessed.</li>
    <li>copy-on-write: copying large or complex objects can be expensive. However copies can be replaced by pointers and only once modified will a copy be made.</li>
</ul>

related to:
<ul>
    <li>adapter: provides a different interface to the object it adapts. A proxy provides the same interface.</li>
    <li>decorator: decorators add more responsabilities whereas proxies control access.</li>
</ul>