package structural.proxy;

public class ComplexOrder implements Order {

    @Override
    public Order copy() {
        System.out.println("complex process to copy order");
        return new ComplexOrder();
    }

    @Override
    public String getAnything() {
        return "Return something important that requires order process to be executed";
    }
}
