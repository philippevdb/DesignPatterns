package structural.proxy;

public interface Order {

    Order copy();

    String getAnything();

}
