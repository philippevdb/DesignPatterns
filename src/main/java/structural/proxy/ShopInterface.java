package structural.proxy;

public class ShopInterface {

    public static void main(String[] args) {
        Order original = new ComplexOrder();
        Order proxy = new OrderProxy(original).copy();
        System.out.println("customer does all kinds of stuff, until the moment finally arrives");
        System.out.println(proxy.getAnything());
    }
}
