Use when:
<ul>
    <li>You want to use existing class and its interface does not match the one you need.</li>
    <li>You want to create a reusable class that cooperates with unrelated or unforeseen classes, that is, classes that don't necessarily have compatible interfaces.</li>
    <li>You need to use several existing subclasses, but it's impractical to adapt their interface by subclassing every one. An object adapter can adapt the interface of its parent class.</li>
</ul>

Consequences:
<ul>
    <li>How much adapting does adapter do? Hugely depends on similar target and adaptee are.</li>
    <li>Pluggable adapters: minimizing assumptions improves usability</li>
</ul>

Similar to:
<ul>
    <li>Bridge: wants to vary the implementation. An adapter changes the interface of an existing object.</li>
    <li>Decorator: enhances another project without changing interface.</li>
    <li>Proxy: defines a representation for another object without changing interface</li>
</ul>