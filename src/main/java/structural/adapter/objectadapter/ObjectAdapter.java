package structural.adapter.objectadapter;

/**
 * Using adapter to allow a landphone to make wireless calls.
 */
public class ObjectAdapter {

    public static void main(String[] args) {
        ILandPhone landPhone = new CellPhoneAdapter(new CellPhone());
        landPhone.callOverWire();
    }
}
