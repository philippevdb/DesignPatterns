package structural.adapter.objectadapter;

public interface ILandPhone {

    void callOverWire();
}
