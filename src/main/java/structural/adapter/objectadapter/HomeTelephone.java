package structural.adapter.objectadapter;

public class HomeTelephone implements ILandPhone {

    @Override
    public void callOverWire() {
        System.out.println(this.getClass().getSimpleName() + ": calls over wire");
    }
}
