package structural.adapter.objectadapter;

public class CellPhoneAdapter implements ILandPhone {

    private CellPhone cellPhone;

    public CellPhoneAdapter(CellPhone cellPhone) {
        this.cellPhone = cellPhone;
    }

    @Override
    public void callOverWire() {
        cellPhone.callWirelessly();
    }
}
