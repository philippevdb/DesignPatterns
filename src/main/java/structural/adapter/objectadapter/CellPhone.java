package structural.adapter.objectadapter;

public class CellPhone {

    public void callWirelessly() {
        System.out.println(this.getClass().getSimpleName() + ": calls wirelessly");
    }
}
