package structural.adapter.classadapter;

/**
 * Not possible in Java. This requires multiple inheritance where you inherit the properties of one and implement the methods of the other.
 * For instance in C++ you can inherit properties of one publicly and the other privately.
 */
public class PhoneAdapter {
}
